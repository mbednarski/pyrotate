import setuptools

with open('README.md', 'r') as f:
    long_description = f.read()
setuptools.setup(
    name='pyrotate',
    version='0.0.1.dev1',
    author='mbednarski',
    author_email='mateusz.bednarski@windowslive.com',
    description='Simple file rotate library for Python',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url='https://gitlab.com/mbednarski/pyrotate',
    packages=setuptools.find_packages(),
    license='MIT',
    python_requires='>=3',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3.0',
        'Topic :: Utilities'
    ]
)
